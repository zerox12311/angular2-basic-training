import { AuthGuard } from './routing.guard';
import { SharedModule } from './shared.module';
import { DataService } from './http.service';


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ComponentComponent } from './component/component.component';
import { InputComponent } from './input/input.component';
import { OutputComponent } from './output/output.component';

import { routes } from './app.routing';

import { ServiceService } from './service.service';
import { Lesson1Component } from './lesson1/lesson1.component';
import { Lesson2Component } from './lesson2/lesson2.component';
import { Lesson3Component } from './lesson3/lesson3.component';
import { Lesson4Component } from './lesson4/lesson4.component';
import { Lesson5Component } from './lesson5/lesson5.component';
import { Lesson6Component } from './lesson6/lesson6.component';
import { Lesson7Component } from './lesson7/lesson7.component';
import { Lesson8Component } from './lesson8/lesson8.component';
import { Lesson9Component } from './lesson9/lesson9.component';
import { TestComponent } from './test/test.component';
import { Lesson10Component } from './lesson10/lesson10.component';
import { Test2Component } from './test2/test2.component';
import { Lesson11Component } from './lesson11/lesson11.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentComponent,
    InputComponent,
    OutputComponent,
    // Lesson1Component,
    Lesson2Component,
    Lesson3Component,
    Lesson4Component,
    Lesson5Component,
    Lesson6Component,
    Lesson7Component,
    Lesson8Component,
    Lesson10Component,
    Test2Component,
    Lesson11Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes,
    SharedModule
  ],
  providers: [ServiceService,DataService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
