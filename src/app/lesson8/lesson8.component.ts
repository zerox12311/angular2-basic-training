import { DataService } from './../http.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lesson8',
  templateUrl: './lesson8.component.html',
  styleUrls: ['./lesson8.component.css']
})
export class Lesson8Component implements OnInit {

  constructor(private _http:DataService) { }

  ngOnInit() {
  }

  getData() {
    this._http.getData().subscribe(
      data => {
        console.log(data);
      },
      error => console.log("ERROR"),
      () => console.log("FINISH")
    )
  }

}
