import { Component, OnInit ,Output , EventEmitter} from '@angular/core';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {


  childVal:string="child";

  @Output() countChange: EventEmitter<string> = new EventEmitter<string>();

  addstr(){
    this.childVal +="_HI";
    this.countChange.emit(this.childVal);
  }
  constructor() { }

  ngOnInit() {
  }

}
