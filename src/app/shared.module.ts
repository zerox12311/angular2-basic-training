import { FormsModule } from '@angular/forms';
import { Lesson1Component } from './lesson1/lesson1.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


@NgModule({
    imports: [
        CommonModule,
        FormsModule
     ],
    declarations: [
         Lesson1Component
    ],
    exports: [
        Lesson1Component
    ]
})

export class SharedModule {}