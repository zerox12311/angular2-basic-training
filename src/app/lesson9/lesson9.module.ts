import { SharedModule } from './../shared.module';
import { TestComponent } from './../test/test.component';
import { routing } from './lesson9.routing';
import { Lesson9Component } from './lesson9.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule
  ],
  declarations: [Lesson9Component,TestComponent]
})
export class Lesson9Module { }


