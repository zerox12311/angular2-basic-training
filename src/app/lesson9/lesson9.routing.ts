import { TestComponent } from './../test/test.component';
import { Lesson9Component } from './lesson9.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



const routes: Routes = [
  {
    path: '', component: Lesson9Component,
    children: [
      { path: 'test', component: TestComponent },
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);


