import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from "@angular/http";
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {
    constructor(private http: Http) {

    }

    getData() {
        return this.http.get("https://restful-test-1a6a1.firebaseio.com/NTUB_CRUD.json").map(res => res.json())
    }

    postData(data) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post("", data, options).map(res => res.json())
    }

    putData(data) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put("", data, options).map(res => res.json())
    }

    delData() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.delete("", options).map(res => res.json())
    }
}
