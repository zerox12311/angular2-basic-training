import { NtubSchoolPage } from './app.po';

describe('ntub-school App', () => {
  let page: NtubSchoolPage;

  beforeEach(() => {
    page = new NtubSchoolPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
